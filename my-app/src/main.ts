import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import "./styles/common.less"
import "./styles/var.less"
import ui from "./plugin/ui.registry"
import baseComponent from "./plugin/baseComponent.registry"
import "./styles/theme.css"

createApp(App).use(router).use(ui).use(baseComponent).mount("#app")
