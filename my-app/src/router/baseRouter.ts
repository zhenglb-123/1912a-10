const baseRouter = [
  // {
  //   path: "/article",
  //   commponent: () => import("views/article/Index.vue"),
  //   meta:{
  //       title:"文章"
  //   }
  // },
  // {
  //   path: "/file",
  //   commponent: () => import("views/file/Index.vue"),
  //   meta:{
  //       title:"归档"
  //   }
  // },
  // {
  //   path: "/knowledge",
  //   commponent: () => import("views/knowledge/Index.vue"),
  //   meta:{
  //       title:"知识小册"
  //   }
  // },
  {
    path: "/home",
    name: "home",
    component: () => import("../views/home/Index.vue"),
    children: [
      {
        path: "/home/article",
        name: "article",
        component: () => import("../views/article/Index.vue"),
      },
      {
        path: "/home/file",
        name: "file",
        component: () => import("../views/file/Index.vue"),
      },
      {
        path: "/home/knowledge",
        name: "knowledge",
        component: () => import("../views/knowledge/Index.vue"),
      },
    ],
  },
  {
    path: "/",
    redirect: "/home/article",
  },
]

export default baseRouter
