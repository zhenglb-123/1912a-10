import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
import baseRouter from "./baseRouter"

const routes: Array<RouteRecordRaw> = [
  ...baseRouter,
  // {
  //   path: "/home",
  //   name: "home",
  //   component: () => import("../views/home/Index.vue"),
  //   children: [
  //     {
  //       path: "/home/article",
  //       name: "article",
  //       component: () => import("../views/article/Index.vue"),
  //     },
  //     {
  //       path: "/home/file",
  //       name: "file",
  //       component: () => import("../views/file/Index.vue"),
  //     },
  //     {
  //       path: "/home/knowledge",
  //       name: "knowledge",
  //       component: () => import("../views/knowledge/Index.vue"),
  //     },
  //   ],
  // },
  // {
  //   path: "/",
  //   redirect: "/home/article",
  // },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
